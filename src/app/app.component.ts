import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    ngOnInit(): void {}

    // title = 'classificador';
    // records: any[];
    // headerArray: any;
    // @ViewChild('csvReader', { static: false }) csvReader: any;
    // uploadListener($event: any): void {
    //   const input = $event.target;
    //   const reader = new FileReader();
    //   reader.readAsText(input.files[0]);
    //   if (this.isValidCSVFile(input.files[0])) {
    //     reader.onload = () => {
    //       const csvData = reader.result;
    //       const csvRecordsArray = (csvData as string).split(/\r\n|\n/);
    //       const headersRow = this.getHeaderArray(csvRecordsArray);
    //       this.records = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length);
    //     };
    //   } else {
    //     this.fileReset();
    //   }
    // }
    // getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any) {
    //   const csvArr = [];
    //   for (let i = 1; i < csvRecordsArray.length; i++) {
    //     const currentRecord = (csvRecordsArray[i] as string).split(',');
    //     if (currentRecord.length === headerLength) {
    //       csvArr.push(currentRecord);
    //     }
    //   }
    //   return csvArr;
    // }
    // isValidCSVFile(file: any) {
    //   return file.name.endsWith('.csv');
    // }
    // getHeaderArray(csvRecordsArr: any) {
    //   const headers = (csvRecordsArr[0] as string).split(',');
    //   this.headerArray = [];
    //   for (const header of headers) {
    //     this.headerArray.push(header);
    //   }
    //   console.log(this.headerArray);
    //   return this.headerArray;
    // }
    // fileReset() {
    //   this.csvReader.nativeElement.value = '';
    //   this.records = [];
    // }
}
