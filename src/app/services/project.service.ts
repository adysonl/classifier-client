import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/project.model';
import { Constants } from '../core/utils/constants';
import { Observable } from 'rxjs';
import { CsvUploadResponse } from '../models/file-upload-response.model';

@Injectable({
    providedIn: 'root'
})
export class ProjectService {
    constructor(private http: HttpClient) {}

    getAll(): Observable<Project[]> {
        return this.http.get<Project[]>(`${Constants.PROJECT_ENDPOINT}`);
    }

    getOne(id): Observable<Project> {
        return this.http.get<Project>(`${Constants.PROJECT_ENDPOINT}/${id}`);
    }

    save(project: Project): Observable<Project> {
        return this.http.post<Project>(`${Constants.PROJECT_ENDPOINT}`, JSON.stringify(project));
    }

    getCsvHeaders(url): Observable<CsvUploadResponse> {
        return this.http.post<CsvUploadResponse>(`${Constants.FILE_UPLOAD_URL}/headers`, {url});
    }
}
