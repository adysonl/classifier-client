import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProjectEditComponent } from './components/project/project-edit/project-edit.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProjectListComponent } from './components/project/project-list/project-list.component';
import { MaterialModule } from './core/material.module';
import { NgxMasonryModule } from 'ngx-masonry';
import { FileSelectDirective } from 'ng2-file-upload';
import { FileUploadComponent } from './core/components/file-upload/file-upload.component';
import { LabelingComponent } from './components/labeling/labeling.component';

@NgModule({
    declarations: [
        AppComponent,
        ProjectEditComponent,
        NavbarComponent,
        ProjectListComponent,
        FileSelectDirective,
        FileUploadComponent,
        LabelingComponent
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MaterialModule,
        NgxMasonryModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
