import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { Constants } from '../../utils/constants';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  @Input() name: string;
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSuccess = new EventEmitter<any>();

  constructor() {
    this.uploader.onAfterAddingFile = file => {
        file.withCredentials = false;
    };
    this.uploader.onCompleteItem = (item: any, response: any, status: any) => {
        console.log('Uploaded File Details:', item);
        this.onSuccess.emit(JSON.parse(response));
    };
}

public uploader: FileUploader = new FileUploader({
    url: Constants.FILE_UPLOAD_URL,
    itemAlias: 'file'
});
  ngOnInit() {
  }

}
