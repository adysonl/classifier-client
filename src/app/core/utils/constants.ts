import { Project } from '../../models/project.model';

export class Constants {
    static readonly PROJECT_MOCK: Project = {
        title: '',
        description: '',
        columns: [],
        data: [],
        labels: []
    };

    static readonly API_URL = 'http://localhost:8080/classifier/api';
    static readonly PROJECT_ENDPOINT = `${Constants.API_URL}/project`;
    static readonly FILE_UPLOAD_URL = `${Constants.API_URL}/file/upload/csv`;
}
