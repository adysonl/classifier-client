import { Component, OnInit } from '@angular/core';
import { Project } from '../../../models/project.model';
import { ProjectService } from '../../../services/project.service';

@Component({
    selector: 'app-project-list',
    templateUrl: './project-list.component.html',
    styleUrls: ['./project-list.component.scss']
})
export class ProjectListComponent implements OnInit {
    constructor(private projectService: ProjectService) {}
    projects: Project[] = [];
    currentData = {
        name: 'nameeeeee',
        age: 12
    };

    ngOnInit() {
        this.projectService.getAll().subscribe(projects => {
            this.projects = projects;
        });
    }
}
