import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Project } from '../../../models/project.model';
import { CsvUploadResponse } from '../../../models/file-upload-response.model';
import { ProjectService } from '../../../services/project.service';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-project-edit',
    templateUrl: './project-edit.component.html',
    styleUrls: ['./project-edit.component.scss']
})
export class ProjectEditComponent implements OnInit {
    projectForm: FormGroup;
    labelFormArray: FormArray;
    columnFormArray: FormArray;
    isEditMode: boolean;

    project: Project;
    columns: string[] = [];
    constructor(private formBuilder: FormBuilder, private projectService: ProjectService, private route: ActivatedRoute) {
        this.project = new Project();
    }
    ngOnInit(): void {
        this.projectForm = this.formBuilder.group({
            title: new FormControl('', [Validators.required]),
            description: new FormControl('', [Validators.required]),
            url: new FormControl('', Validators.required)
        });

        this.labelFormArray = this.formBuilder.array([this.createLabelControl()]);
        this.columnFormArray = this.formBuilder.array([this.createColumnControl()]);

        this.route.params.subscribe(params => {
            if (params.id) {
                this.projectService.getOne(params.id).subscribe(project => {
                    this.project = project;

                    this.projectService.getCsvHeaders(this.project.url).subscribe(result => {
                        this.columns = result.headers;

                        this.projectForm.patchValue({
                            title: this.project.title,
                            description: this.project.description,
                            url: this.project.url
                        });
                        this.labelFormArray.removeAt(0);
                        this.columnFormArray.removeAt(0);
                        this.project.labels.forEach(label => {
                            this.labelFormArray.push(this.createLabelControl(label));
                        });
                        this.project.columns.forEach(column => {
                            this.columnFormArray.push(this.createColumnControl(column));
                        });
                    });
                });
            }
        });
    }

    submit() {
        this.project.title = this.projectForm.value.title;
        this.project.description = this.projectForm.value.description;
        this.project.url = this.projectForm.value.url;
        this.project.labels = this.labelFormArray.value;
        this.project.columns = this.columnFormArray.value;
        if (this.project._id) {
            this.update();
        } else {
            this.save();
        }
    }

    save() {
        console.log('save');
        this.projectService.save(this.project).subscribe(response => console.log(response));
    }

    update() {
        console.log('update');
    }
    createLabelControl(label?): FormGroup {
        return this.formBuilder.group({
            _id: new FormControl(label ? label._id : undefined),
            name: new FormControl(label ? label.name : undefined, [Validators.required]),
            key: new FormControl(label ? label.key : undefined, [Validators.required]),
            value: new FormControl(label ? label.value : undefined, [Validators.required])
        });
    }

    createColumnControl(column?): FormGroup {
        return this.formBuilder.group({
            _id: new FormControl(column ? column._id : undefined),
            name: new FormControl(column ? column.name : undefined, [Validators.required]),
            field: new FormControl(column ? column.field : undefined, [Validators.required])
        });
    }
    addLabel(): void {
        this.labelFormArray.push(this.createLabelControl());
    }

    addColumn(): void {
        this.columnFormArray.push(this.createColumnControl());
    }

    removeLabel(index: number): void {
        this.labelFormArray.removeAt(index);
    }

    removeColumn(index: number): void {
        this.columnFormArray.removeAt(index);
    }

    updateColumns(response: CsvUploadResponse) {
        this.projectForm.controls.url.setValue(response.url);
        this.columns = response.headers;
    }

    setColumnToForm(value: string, formIndex: number) {
        (this.columnFormArray.controls[formIndex] as FormGroup).controls.field.setValue(value);
    }
}
