import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';
import { ActivatedRoute } from '@angular/router';
import { Project } from '../../models/project.model';
import { Label } from '../../models/label.model';
import { Column } from '../../models/column.model';

@Component({
    selector: 'app-labeling',
    templateUrl: './labeling.component.html',
    styleUrls: ['./labeling.component.scss']
})
export class LabelingComponent implements OnInit {
    project: Project;
    labels: Label[];
    columns: Column[];
    currentData = {
        name: 'nameeeeee',
        age: 12
    };

    constructor(private projectService: ProjectService, private route: ActivatedRoute) {}

    ngOnInit() {
        this.route.params.subscribe(params => {
            if (params.id) {
                this.projectService.getOne(params.id).subscribe(project => {
                    this.project = project;
                    this.labels = this.project.labels;
                    this.columns = this.project.columns;
                });
            }
        });
    }
}
