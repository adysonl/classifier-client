import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectEditComponent } from './components/project/project-edit/project-edit.component';
import { ProjectListComponent } from './components/project/project-list/project-list.component';
import { LabelingComponent } from './components/labeling/labeling.component';


const routes: Routes = [
  {
    path: 'project/new',
    component: ProjectEditComponent
  },
  {
    path: 'project/edit/:id',
    component: ProjectEditComponent
  },
  {
    path: 'project',
    component: ProjectListComponent
  },
  {
    path: 'project/:id/labeling',
    component: LabelingComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
