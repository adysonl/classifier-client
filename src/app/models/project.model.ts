import { User } from './user.model';
import { Label } from './label.model';
import { Data } from './data.model';
import { Column } from './column.model';

export class Project {
    // tslint:disable-next-line: variable-name
    _id?: string;
    title: string;
    description?: string;
    file?: File;
    columns: Column[] = [];
    data: Data[] = [];
    labels: Label[] = [];
    author?: User;
    image?: string;
    url?: string;
}
