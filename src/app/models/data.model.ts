import { DataTypeEnum } from '../core/utils/enums';

export class Data {
    values: any[];
    classification: number[];
    type: DataTypeEnum;
}
