import { DataTypeEnum } from '../core/utils/enums';

export class Column {
    field: string;
    name: string;
    dataType: DataTypeEnum;
}
