import { DataTypeEnum } from '../core/utils/enums';

export class Label {
    key: string;
    name: string;
    color?: string;
    icon?: string;
    value?: any;
    type?: DataTypeEnum;
}
