export class CsvUploadResponse {
    url: string;
    headers: string[];
}
